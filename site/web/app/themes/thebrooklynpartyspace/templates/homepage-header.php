<?php use Roots\Sage\Titles; ?>
<?php use Roots\Sage\Assets; ?>

<div class="section section-header homepage-header">
    <div class="parallax pattern-image"> 
        <!--<img src="assets/img/rubik_background.jpg"/>-->
        
        <div class="container"> 
             <div class="content">
                <?php if ( get_theme_mod( 'theme_homepage_header_img' ) ) : ?>
                    <div id="homepage-header-img" class='homepage-header-img'>
                        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'theme_homepage_header_img' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
                    </div>
                <?php else : ?>
                    <hgroup>
                        <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
                        <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
                    </hgroup>
                <?php endif; ?>
            </div>               
        </div>
        <a href="javascript:void(0)" data-scroll="true" data-id="#whoWeAre" class="scroll-arrow hidden-xs hidden-sm">
            <i class="fa fa-angle-down"></i>
        </a>
    </div>
</div>
