<div class="container">
<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<!-- Section "Our Projects" example 4: 3 Columns Boxed & Color Hover -->
    <div class="section section-we-made-3 section-with-hover" id="projects1">
      <div class="text-area">
          <div class="title add-animation-stopped">
              <h5 class="text-gray">3 Columns Boxed & Color Hover</h5>
              <h2>Our Projects</h2>
              <div class="separator-container">
                  <div class="separator line-separator">✻</div>
              </div>
              <p>In this area you can write some interesting description about the projects that your team created. Don't forget to add some awesome images under this description, like the beautiful ones that we added.</p>
          </div>
      </div>
      <div class="container">
          <div class="row" id="projectsLine1">

<!-- End Section "Our Projects" example 4: 3 Columns Boxed & Color Hover -->
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'portfolio_project'); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
</div>
          </div>
      </div>
    </div>