<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */

function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');

/**
 * Header Logo Customizer
 */

function customize_homepage_header_image($wp_customize) {
    $wp_customize->add_section( 'theme_homepage_section' , array(
        'title'       => __( 'Homepage', 'theme' ),
        'priority'    => 30,
        'description' => 'Upload an image to replace the title in the homepage header.',
        )
    );
    $wp_customize->add_setting( 'theme_homepage_header_img' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'theme_homepage_header_img', array(
                'label'    => __( 'Header Image', 'themeslug' ),
                'section'  => 'theme_homepage_section',
                'settings' => 'theme_homepage_header_img',
            ) 
        ) 
    );
}

add_action( 'customize_register', __NAMESPACE__ . '\\customize_homepage_header_image' );

/**
 * Homepage Featured Galleries
 */

function customize_homepage_featured_galleries($wp_customize) {
    // Gallery 1
    
    $wp_customize->add_setting('theme_featured_gallery1_title');
    $wp_customize->add_control(
        'theme_featured_gallery1_title',
        array(
            'label'     => __('Featured Gallery 1 Title', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery1_title',
            'type'      => 'text'
        )
    );

    $wp_customize->add_setting( 'theme_featured_gallery1_img' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'theme_featured_gallery1_img', array(
                'label'    => __( 'Featured Gallery 1 Image', 'themeslug' ),
                'section'  => 'theme_homepage_section',
                'settings' => 'theme_featured_gallery1_img',
            ) 
        ) 
    );

    $wp_customize->add_setting('theme_featured_gallery1_link');
    $wp_customize->add_control(
        'theme_featured_gallery1_link',
        array(
            'label'     => __('Featured Gallery 1 Link', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery1_link',
            'type'      => 'dropdown-pages'
        )
    );

    //Gallery 2

    $wp_customize->add_setting('theme_featured_gallery2_title');
    $wp_customize->add_control(
        'theme_featured_gallery2_title',
        array(
            'label'     => __('Featured Gallery 2 Title', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery2_title',
            'type'      => 'text'
        )
    );

    $wp_customize->add_setting( 'theme_featured_gallery2_img' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'theme_featured_gallery2_img', array(
                'label'    => __( 'Featured Gallery 2 Image', 'themeslug' ),
                'section'  => 'theme_homepage_section',
                'settings' => 'theme_featured_gallery2_img',
            ) 
        ) 
    );

    $wp_customize->add_setting('theme_featured_gallery2_link');
    $wp_customize->add_control(
        'theme_featured_gallery2_link',
        array(
            'label'     => __('Featured Gallery 2 Link', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery2_link',
            'type'      => 'dropdown-pages'
        )
    );

    // Gallery 3

    $wp_customize->add_setting('theme_featured_gallery3_title');
    $wp_customize->add_control(
        'theme_featured_gallery3_title',
        array(
            'label'     => __('Featured Gallery 3 Title', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery3_title',
            'type'      => 'text'
        )
    );

    $wp_customize->add_setting( 'theme_featured_gallery3_img' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'theme_featured_gallery3_img', array(
                'label'    => __( 'Featured Gallery 3 Image', 'themeslug' ),
                'section'  => 'theme_homepage_section',
                'settings' => 'theme_featured_gallery3_img',
            ) 
        ) 
    );

    $wp_customize->add_setting('theme_featured_gallery3_link');
    $wp_customize->add_control(
        'theme_featured_gallery3_link',
        array(
            'label'     => __('Featured Gallery 3 Link', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery3_link',
            'type'      => 'dropdown-pages'
        )
    );

    //Gallery 4

    $wp_customize->add_setting('theme_featured_gallery4_title');
    $wp_customize->add_control(
        'theme_featured_gallery4_title',
        array(
            'label'     => __('Featured Gallery 4 Title', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery4_title',
            'type'      => 'text'
        )
    );

    $wp_customize->add_setting( 'theme_featured_gallery4_img' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'theme_featured_gallery4_img', array(
                'label'    => __( 'Featured Gallery 4 Image', 'themeslug' ),
                'section'  => 'theme_homepage_section',
                'settings' => 'theme_featured_gallery4_img',
            ) 
        ) 
    );

    $wp_customize->add_setting('theme_featured_gallery4_link');
    $wp_customize->add_control(
        'theme_featured_gallery4_link',
        array(
            'label'     => __('Featured Gallery 4 Link', 'theme'),
            'section'   => 'theme_homepage_section',
            'settings'  => 'theme_featured_gallery4_link',
            'type'      => 'dropdown-pages'
        )
    );

}

add_action( 'customize_register', __NAMESPACE__ . '\\customize_homepage_featured_galleries' );

/**
 * Contact & Social Customizer
 */

function customize_contact_info($wp_customize) {
    $wp_customize->add_section( 'theme_contact_section' , array(
        'title'       => __( 'Contact & Social Media', 'theme' ),
        'priority'    => 20,
        'description' => 'Enter your contace and social media info.',
        )
    );
    
    //Social media settings
    
    //Facebook
    $wp_customize->add_setting('theme_facebook_link');
    $wp_customize->add_control(
        'theme_facebook_link',
        array(
            'label'     => __('Facebook Link', 'theme'),
            'section'   => 'theme_contact_section',
            'settings'  => 'theme_facebook_link',
            'type'      => 'text'
        )
    );

    //Twitter
    $wp_customize->add_setting('theme_twitter_link');
    $wp_customize->add_control(
        'theme_twitter_link',
        array(
            'label'     => __('Twitter Link', 'theme'),
            'section'   => 'theme_contact_section',
            'settings'  => 'theme_twitter_link',
            'type'      => 'text'
        )
    );
    
    //Instagram
    $wp_customize->add_setting('theme_instagram_link');
    $wp_customize->add_control(
        'theme_instagram_link',
        array(
            'label'     => __('Instagram Link', 'theme'),
            'section'   => 'theme_contact_section',
            'settings'  => 'theme_instagram_link',
            'type'      => 'text'
        )
    );


}

add_action( 'customize_register', __NAMESPACE__ . '\\customize_contact_info' );
/**
 * Gallery Link Customizer
 */