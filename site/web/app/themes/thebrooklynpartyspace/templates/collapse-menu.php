<div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
        <li class="social-links">
            <a href="#"> 
                <i class="fa fa-facebook"></i>
            </a>
            <a href="#"> 
                <i class="fa fa-twitter"></i>
            </a>
            <a href="#"> 
                <i class="fa fa-instagram"></i>
            </a>
        </li>
   </ul>
    <?php 
        if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array(
                'theme_location' => 'primary_navigation',
                'container' => false,
                'menu_class' => 'nav navbar-nav navbar-right navbar-uppercase'
            ));
        endif;
    ?>

</div>