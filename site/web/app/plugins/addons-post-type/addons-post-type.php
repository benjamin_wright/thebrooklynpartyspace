<?php
/**
 * Plugin Name: Add-ons Post Type
 * Description: This plugin creates an addon-post type for extra event features. It includes the jquery flip plugin for creating flip items.
 * Version:     1.0
 * Author:      Benjamin Wright
 * Author URI:  http://www.famousstick.com
 * Text Domain: addon-post-type
 * Domain Path: /languages
 */

class Addons_PostType {

    /**
     * Directory path to the plugin folder.
     *
     * @since  1.0.0
     * @access public
     * @var    string
     */
    public $dir_path = '';

    /**
     * Directory URI to the plugin folder.
     *
     * @since  1.0.0
     * @access public
     * @var    string
     */
    public $dir_uri = '';

    /**
     * JavaScript directory URI.
     *
     * @since  1.0.0
     * @access public
     * @var    string
     */
    public $js_uri = '';

    /**
     * CSS directory URI.
     *
     * @since  1.0.0
     * @access public
     * @var    string
     */
    public $css_uri = '';   
 
    public static function get_instance() {
        
        static $instance = null;

        if ( is_null( $instance ) ) {
            $instance = new self;
            $instance->setup();
            $instance->includes();
            $instance->setup_actions();
        }

        return $instance;

    }

    private function __construct() {}

    private function setup() {

        $this->dir_path = trailingslashit( plugin_dir_path( __FILE__ ) );
        $this->dir_uri  = trailingslashit( plugin_dir_url(  __FILE__ ) );

        $this->js_uri  = trailingslashit( $this->dir_uri . 'js'  );
        $this->css_uri = trailingslashit( $this->dir_uri . 'css' );
    }

    private function includes() {

        // Load functions files.
        require_once( $this->dir_path . 'inc/functions.php'    );
    }

    /**
     * Sets up initial actions.
     *
     * @since  1.0.0
     * @access private
     * @return void
     */
    private function setup_actions() {


        // Register activation hook.
        register_activation_hook( __FILE__, array( $this, 'activation' ) );
    }

}

function addons_posttype() {
    return Addons_PostType::get_instance();
}

addons_posttype();