
<article <?php post_class("col-md-4"); ?>class="">
    <div class="project add-animation-stopped animation-1">
        <? the_post_thumbnail( 'large' ); ?>
        <a class="over-area color-2" href="javascript:void(0)" onClick="rubik.showModal(this)" data-target="project_<?php echo $post->ID; ?>">
            <div class="content">
                <h4 class="entry-title"><?php the_title(); ?></h4>
            </div>
        </a>
    </div>
</article>