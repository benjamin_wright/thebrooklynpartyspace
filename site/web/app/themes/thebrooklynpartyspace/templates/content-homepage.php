<?php the_content(); ?>

<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
<!-- SECTION WHO WE ARE -->
    <div class="section section-we-are-1" id="whoWeAre">
        <div class="text-area">
            <div class="container">
                <div class="row">
                    <div class="title add-animation animate" id="animationTest">
                        <h2>We understand that each client has different needs…</h2>
                        <div class="separator-container">
                            <div class="separator line-separator">♦</div>
                        </div>
                        <p class="large"><strong>That’s why we strive to serve your unique requests. If you are searching for something that is not on our site, just ask! We’ll be more than happy to help.</strong></p>
                        <p class="large">
                            Brooklyn Party Space is a 3,000 sq ft, very cool, club-style event space for those parties that need more than just a banquet hall. You will find that the balance between chic and elegance is tastefully crafted. Our venue is suitable for many occasions including birthday parties, baby showers, bridal showers, fashion shows, weddings, and corporate events. Whatever your occasion, you can count on us to make your event a success! We have EXTREMELY reasonable rates and a strong desire to offer clients the best quality and professional service. Come check out why so many of our clients are saying that our spot is the hottest around!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax pattern-image hidden-xs">
            <img class="parallax-img" src="<?= get_template_directory_uri(); ?>/dist/images/anniversary_background.jpg" style="transform: translate3d(0px, -227.844px, 0px);">
        </div>
    </div>
<!-- End Section - Who We Are -->

<!-- SECTION WHAT'S INCLUDED -->
  <div class="section section-we-are-2">
      <div class="text-area">
          <div class="container">
              <div class="row">
                <div class="title add-animation animate" id="animationTest">
                    <h2>What Is Included</h2>
                    <div class="separator-container">
                        <div class="separator line-separator">♦</div>
                    </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="title add-animation-stopped animate">
                          <h3>Tables &amp; Catering Supplies</h3>
                      </div>
                  </div>
                  <div class="col-md-7 col-md-offset-1">
                      <div class="description add-animation-stopped animation-1 animate">
                          <p>We're The Company, a small design agency based in Chicago. We’ve been crafting beautiful websites, launching stunning brands and making clients happy for years.</p>
                          <p>With our prestigious craftsmanship, remarkable client care and passion for design, you could say we’re the ‘all singing, all dancing’ kind…</p>
                          <p>We think you’ll love working with us.</p>
                      </div>
                  </div>
                  <div class="separator-container">
                      <div class="separator line-separator">✻</div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="title add-animation-stopped animate">
                          <h3>Courtesy Products For Vanity Table</h3>
                      </div>
                  </div>
                  <div class="col-md-7 col-md-offset-1">
                      <div class="description add-animation-stopped animation-1 animate">
                          <p>We're The Company, a small design agency based in Chicago. We’ve been crafting beautiful websites, launching stunning brands and making clients happy for years.</p>
                          <p>With our prestigious craftsmanship, remarkable client care and passion for design, you could say we’re the ‘all singing, all dancing’ kind…</p>
                          <p>We think you’ll love working with us.</p>
                      </div>
                  </div>
                  <div class="separator-container">
                      <div class="separator line-separator">✻</div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="title add-animation-stopped animate">
                          <h3>DJ Equipment</h3>
                      </div>
                  </div>
                  <div class="col-md-7 col-md-offset-1">
                      <div class="description add-animation-stopped animation-1 animate">
                          <p>We're The Company, a small design agency based in Chicago. We’ve been crafting beautiful websites, launching stunning brands and making clients happy for years.</p>
                          <p>With our prestigious craftsmanship, remarkable client care and passion for design, you could say we’re the ‘all singing, all dancing’ kind…</p>
                          <p>We think you’ll love working with us.</p>
                      </div>
                  </div>
                  <div class="separator-container">
                      <div class="separator line-separator">✻</div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="title add-animation-stopped animate">
                          <h3>Other</h3>
                      </div>
                  </div>
                  <div class="col-md-7 col-md-offset-1">
                      <div class="description add-animation-stopped animation-1 animate">
                          <p>We're The Company, a small design agency based in Chicago. We’ve been crafting beautiful websites, launching stunning brands and making clients happy for years.</p>
                          <p>With our prestigious craftsmanship, remarkable client care and passion for design, you could say we’re the ‘all singing, all dancing’ kind…</p>
                          <p>We think you’ll love working with us.</p>
                      </div>
                  </div>
                  <div class="separator-container">
                      <div class="separator line-separator">✻</div>
                  </div>
              </div>
          </div>
      </div>
      <div class="parallax full-image pattern-image">
          <img alt="..." class="parallax-img" src="<?= get_template_directory_uri(); ?>/dist/images/babyshower_background.jpg" style="transform: translate3d(0px, -150.688px, 0px);">
      </div>
  </div>
<!-- End Section What's Included -->

<!-- SECTION WHAT WE MADE -->
<!-- Section "Our Projects" example 3: 2 Columns Boxed -->
  <div class="section section-we-made-3 section-with-hover" id="projects1">
      <div class="text-area">
          <div class="title add-animation-stopped">
              <h2>Types Of Events</h2>
              <div class="separator-container">
                  <div class="separator line-separator">✻</div>
              </div>
              <p>In this area you can write some interesting description about the projects that your team created. Don't forget to add some awesome images under this description, like the beautiful ones that we added.</p>
          </div>
      </div>
      <div class="container">
          <div class="row" id="projectsLine1">
              <div class="col-md-6">
                  <div class="project add-animation-stopped animation-1">
                      <?php if ( get_theme_mod( 'theme_homepage_header_img' ) ) : ?>
                        <img alt="..." src="<?php echo esc_url( get_theme_mod( 'theme_featured_gallery1_img' ) ); ?>"/>
                        <?php else : ?>
                        <img alt="..." src="assets/img/builder/projects/project.jpg"/>
                        <?php endif; ?>
                      <a class="over-area color-1" href="<?php get_theme_mod( 'theme_featured_gallery1_link'); ?>" >
                          <div class="content">
                              <h4><?php echo get_theme_mod( 'theme_featured_gallery1_title' ); ?></h4>
                              <p>Click to find out more</p>
                          </div>
                      </a>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="project add-animation-stopped animation-1">
                      <?php if ( get_theme_mod( 'theme_homepage_header_img' ) ) : ?>
                        <img alt="..." src="<?php echo esc_url( get_theme_mod( 'theme_featured_gallery2_img' ) ); ?>"/>
                        <?php else : ?>
                        <img alt="..." src="assets/img/builder/projects/project.jpg"/>
                        <?php endif; ?>
                      <a class="over-area color-1" href="<?php get_theme_mod( 'theme_featured_gallery2_link'); ?>" >
                          <div class="content">
                              <h4><?php echo get_theme_mod( 'theme_featured_gallery2_title' ); ?></h4>
                              <p>Click to find out more</p>
                          </div>
                      </a>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="project add-animation-stopped animation-1">
                      <?php if ( get_theme_mod( 'theme_homepage_header_img' ) ) : ?>
                        <img alt="..." src="<?php echo esc_url( get_theme_mod( 'theme_featured_gallery3_img' ) ); ?>"/>
                        <?php else : ?>
                        <img alt="..." src="assets/img/builder/projects/project.jpg"/>
                        <?php endif; ?>
                      <a class="over-area color-1" href="<?php get_theme_mod( 'theme_featured_gallery3_link'); ?>" >
                          <div class="content">
                              <h4><?php echo get_theme_mod( 'theme_featured_gallery3_title' ); ?></h4>
                              <p>Click to find out more</p>
                          </div>
                      </a>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="project add-animation-stopped animation-1">
                      <?php if ( get_theme_mod( 'theme_homepage_header_img' ) ) : ?>
                        <img alt="..." src="<?php echo esc_url( get_theme_mod( 'theme_featured_gallery4_img' ) ); ?>"/>
                        <?php else : ?>
                        <img alt="..." src="assets/img/builder/projects/project.jpg"/>
                        <?php endif; ?>
                      <a class="over-area color-1" href="<?php get_theme_mod( 'theme_featured_gallery4_link'); ?>" >
                          <div class="content">
                              <h4><?php echo get_theme_mod( 'theme_featured_gallery4_title' ); ?></h4>
                              <p>Click to find out more</p>
                          </div>
                      </a>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- End Section "Our Projects" example 3: 2 Columns Boxed -->

  <!-- SECTION PRICING -->
    <div class="section section-we-are-2" id="Pricing">
        <div class="parallax full-image pattern-image">
            <img class="parallax-img" src="<?= get_template_directory_uri(); ?>/dist/images/birthday-background.jpg" style="transform: translate3d(0px, -227.844px, 0px);">
        </div>
        <div class="text-area">
            <div class="container">
                <div class="row">
                    <div class="title add-animation animate" id="animationTest">
                        <h2>Pricing</h2>
                        <div class="separator-container">
                            <div class="separator line-separator">♦</div>
                        </div>
                        <p class="large"><strong>That’s why we strive to serve your unique requests. If you are searching for something that is not on our site, just ask! We’ll be more than happy to help.</strong></p>
                        <p class="large">
                            Brooklyn Party Space is a 3,000 sq ft, very cool, club-style event space for those parties that need more than just a banquet hall. You will find that the balance between chic and elegance is tastefully crafted. Our venue is suitable for many occasions including birthday parties, baby showers, bridal showers, fashion shows, weddings, and corporate events. Whatever your occasion, you can count on us to make your event a success! We have EXTREMELY reasonable rates and a strong desire to offer clients the best quality and professional service. Come check out why so many of our clients are saying that our spot is the hottest around!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax full-image pattern-image">
            <img class="parallax-img" src="<?= get_template_directory_uri(); ?>/dist/images/wedding_background.jpg" style="transform: translate3d(0px, -227.844px, 0px);">
        </div>
    </div>
  <!-- end section pricing -->
  
  <!-- SECTION PACKAGES -->
<div class="section section-we-do-2" id="workflow">
    <div class="container">
        <div class="row">
            <div class="title add-animation animate">
                <h2>Packages &amp; Add-Ons</h2>
                <div class="separator-container">
                    <div class="separator line-separator">♦</div>
                </div>
                <p>We promise you a new look and more importantly, a new attitude. We build that by getting to know you, your needs and creating the best looking clothes.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card add-animation animation-1 animate">
                    <div class="icon">
                        <i class="pe-7s-paint"></i>
                    </div>
                    <h3>Design</h3>
                    <p>We sketch your wardrobe down to the last detail and present it to you.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card add-animation animation-2 animate">
                    <div class="icon">
                        <i class="pe-7s-scissors"></i>
                    </div>
                    <h3>Adjustments</h3>
                    <p>We make our design perfect for you. Our adjustment turn our clothes into your clothes.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card add-animation animation-3 animate">
                    <div class="icon">
                        <i class="pe-7s-plugin"></i>
                    </div>
                    <h3>Branding</h3>
                    <p>We create a persona regarding the multiple wardrobe accessories that we provide..</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card add-animation animation-4 animate">
                    <div class="icon">
                        <i class="pe-7s-piggy"></i>
                    </div>
                    <h3>Marketing</h3>
                    <p>We like to present the world with our work, so we make sure we spread the word regarding our clothes.</p>
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- END SECTION PACKAGES -->

  <!-- Contact Us Section -->
<div class="section section-contact-1" id="contact">
    <div class="container">
        <div class="text-area">
            <div class="title add-animation animate">
                <h2>Contact Us</h2>
                <div class="separator-container">
                    <div class="separator line-separator">♦</div>
                </div>
                <p>Find us in the heart of Paris in an old brick block of flats</p>
            </div>
            <div class="contact-form">
                <div class="row">
                    <div class="col-md-3 col-sm-3 right-border hidden-xs">
                        <div class="address add-animation add-animation-1 animate">
                            <h4>Where to meet ?</h4>
                            <p class="text-gray">
                                2578 Paris<br>
                                Rue Abel, 12<br>
                                France
                            </p>
                            <h4>Phone</h4>
                            <p class="text-gray">0456 / 71 21 39</p>
                            <h4>By the old way</h4>
                            <a href="mailto:hello@creative-tim.com">
                                <p class="text-gray">hello@creativetim.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-7 col-sm-offset-2">
                        <div class="form-group add-animation animation-1 animate">
                            <input type="text" value="" placeholder="Your Full Name" class="form-control">
                        </div>
                        <div class="form-group add-animation animation-2 animate">
                            <input type="text" value="" placeholder="Your Email" class="form-control">
                        </div>
                        <div class="form-group add-animation animation-3 animate">
                            <textarea class="form-control" placeholder="Here you can write some nice text" rows="8"></textarea>
                        </div>
                        <button class="btn btn-lg btn-black pull-right">
                        SEND <i class="fa fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- End Contact Us Section -->
