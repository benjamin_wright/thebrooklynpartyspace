<?php
// Plugin functions

function custom_post_addons() {
  $labels = array(
    'name'               => _x( 'Add-Ons', 'post type general name' ),
    'singular_name'      => _x( 'Add-On', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Add-On' ),
    'edit_item'          => __( 'Edit Add-On' ),
    'new_item'           => __( 'New Add-On' ),
    'all_items'          => __( 'All Add-Ons' ),
    'view_item'          => __( 'View Add-On' ),
    'search_items'       => __( 'Search Add-Ons' ),
    'not_found'          => __( 'No products found' ),
    'not_found_in_trash' => __( 'No products found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Add-Ons'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our products and product specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'addon', $args ); 
}
add_action( 'init', 'custom_post_addons' );