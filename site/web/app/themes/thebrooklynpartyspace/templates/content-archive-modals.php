    <!-- If this is a portfolio item taxonomy page, output the modal popups for each post. -->
    <!-- http://wordpress.stackexchange.com/questions/102346/how-to-display-post-from-current-taxonomy-in-archive-page -->
    <?php if (is_tax( 'portfolio_category' )) : ?>
        <?php 
            $qobj = get_queried_object();
             // concatenate the query
            $args = array(
              'posts_per_page' => 1,
              'orderby' => 'rand',
              'tax_query' => array(
                array(
                  'taxonomy' => $qobj->taxonomy,
                  'field' => 'id',
                  'terms' => $qobj->term_id,
            //    using a slug is also possible
            //    'field' => 'slug', 
            //    'terms' => $qobj->name
                )
              )
            );
            $portfolio_query = new WP_Query( $args );
            // var_dump($random_query); // debugging only
        ?>
        <?php if ($portfolio_query->have_posts()) : ?>
            <?php while ($portfolio_query->have_posts()) : ?>
                  <?php $portfolio_query->the_post(); ?>

                    <div class="project-content" id="project_<?php echo $post->ID; ?>">
                      <div>
                        <div class="project-details">
                          <span class="icon-close"><i class="pe-7s-close-circle"></i></span>
                          <div class="container">
                            <div class="project-title">
                              <h2><?php echo get_the_title(); ?></h2>
                              <div class="separator-container">
                                  <div class="separator line-separator">♦</div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-10 col-md-offset-1">
                                <div class="article">
                                  <div class="project-image">
                                     <? the_post_thumbnail( 'large' ); ?>
                                  </div>
                                  <div class="project-text">
                                    <?php the_content(); ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
            <?php endwhile; ?>
        <?php endif; ?>
    <?php endif; ?>