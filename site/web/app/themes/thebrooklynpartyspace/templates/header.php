  <nav class="navbar navbar-default navbar-transparent navbar-fixed-top navbar-burger" role="navigation">
    <!-- if you want to keep the navbar hidden you can add this class to the navbar "navbar-burger"-->
    
    <div class="container">
    <div class="navbar-header">
        <?php 
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(array(
                    'theme_location' => 'primary_navigation',
                    'container' => false,
                    'menu_class' => 'nav navbar-nav navbar-right navbar-uppercase'
                ));
            endif;
        ?>
        <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar bar1"></span>
            <span class="icon-bar bar2"></span>
            <span class="icon-bar bar3"></span>
        </button>
        <a href="<?php echo bloginfo('url') ?>" class="navbar-brand">
          <?php echo bloginfo('name') ?>
        </a>
    </div>


    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right navbar-uppercase">
            <li class="social-links">
                <?php if ( get_theme_mod( 'theme_facebook_link' ) ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'theme_facebook_link' ) ); ?>"> 
                    <i class="fa fa-facebook"></i>
                </a>
                <?php endif; ?>
                <?php if ( get_theme_mod( 'theme_twitter_link' ) ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'theme_twitter_link' ) ); ?>"> 
                    <i class="fa fa-twitter"></i>
                </a>
                <?php endif; ?>
                <?php if ( get_theme_mod( 'theme_instagram_link' ) ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'theme_instagram_link' ) ); ?>"> 
                    <i class="fa fa-instagram"></i>
                </a>
                <?php endif; ?>
            </li>

       </ul>
        <?php 
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(array(
                    'theme_location' => 'primary_navigation',
                    'container' => false,
                    'menu_class' => 'nav navbar-nav navbar-right navbar-uppercase'
                ));
            endif;
        ?>

    </div><!-- /.navbar-collapse -->

  </div>
</nav>
<header class="banner">

</header>
